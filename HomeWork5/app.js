/* Завдання */
// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.

// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.


const urlUser = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

const newsContainer = document.createElement('div');
newsContainer.style.width = '800px';
newsContainer.style.margin = '0 auto';
document.body.insertAdjacentElement('afterbegin', newsContainer);

class Card {
    constructor(title, content, userName, userEmail) {
        this.title = title,
        this.content = content,
        this.userName = userName,
        this.userEmail = userEmail
    }

    createCard() {
        return `
        <div>
            <p><a href =""> ${this.userName} <br> ${this.userEmail} </a></p>
            <h4> ${this.title} </h4>
            <p> ${this.content} </p>
        </div>
        `  
    }
}

function getUsers(url) {
    return fetch(url)
}

function getPosts(url) {
    return fetch(url)
}

let fragment = '';
getUsers(urlUser)
    .then(data => data.json())
    .then(users => users.forEach(user => {
        getPosts(urlPosts)
            .then(data => data.json())
            .then(posts => posts.forEach(post => {
                if(post.userId === user.id) {
                    const article = document.createElement('div');
                    article.style.paddingBottom = '20px';
                    article.style.borderBottom = '1px solid #000';
                    article.setAttribute('data-id', post.id);

                    fragment = new Card(post.title, post.body, user.name, user.email).createCard();
                    article.insertAdjacentHTML('beforeend', fragment);

                    const btnDelete = createDeleteBtn('data-id', post.id);
                    // btnDelete.setAttribute('btn-id', post.id)
                    article.insertAdjacentElement('beforeend', btnDelete);
                    
                    newsContainer.insertAdjacentElement('beforeend', article);
                }
            }))
        }))

function createDeleteBtn(attribute, value) {
    const btnDelete = document.createElement('button');
    btnDelete.innerText = 'Delete';
    btnDelete.setAttribute(attribute, value);
    return btnDelete;
}

document.addEventListener('click', ev => {
    if(ev.target.tagName.toLowerCase() === 'button' && ev.target.dataset.id) {
        deletePost(ev.target.dataset.id);
    }
})

function deletePost(id) {
    if (confirm('Are you sure you want to delete this post?')) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
            method: 'DELETE',
        })
            .then(response => {
                if (response.status === 200) {
                    const cardToDelete = document.querySelector(`[data-id='${id}']`)
                    newsContainer.removeChild(cardToDelete)
                }
            })
    }
}
