/* Завдання */
// Дано масив books.
// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const div = document.querySelector('#root');
const ul = document.createElement('ul');
function showList(arr) {
    arr.forEach(element => {
        try {
            if(!element.name) {
                throw new Error(`Name is absent for the book written by ${element.author} with the price of ${element.price}`);
            };
            if(!element.author) {
                throw new Error(`Author is absent for the book "${element.name}" with the price of ${element.price}`);
            };
            if(!element.price) {
                throw new Error(`Price is absent for the book "${element.name}" written by ${element.author}`);
            }

            const li = document.createElement('li');
            for (let i = 0; i < Object.keys(element).length; i++) {
                const p = document.createElement('p');
                p.textContent = Object.keys(element)[i] + ': ' + Object.values(element)[i] + ',';
                li.append(p);
            }
            ul.append(li);
            
        } catch (error) {
            console.log(error.message);
        }
    })
    div.append(ul);
    return div;
}

// function showList(arr) {
//     for (let j = 0;  j < arr.length; j++) {
//         try {
//             if(!arr[j].name) {
//                 throw new Error(`Book ${[j+1]} has no name`);
//             };
//             if(!arr[j].author) {
//                 throw new Error(`Book ${[j+1]} has no author`);
//             };
//             if(!arr[j].price) {
//                 throw new Error(`Book ${[j+1]} has no price`);
//             }
//             const li = document.createElement('li');
//                 for (let i = 0; i < Object.keys(arr[j]).length; i++) {
//                     const p = document.createElement('p');
//                     p.textContent = Object.keys(arr[j])[i] + ': ' + Object.values(arr[j])[i] + ',';
//                     li.append(p);
//                 }
//             ul.append(li);
//         } catch (error) {
//             console.log(error.message);
//         }
//     }
//     div.append(ul);
//     return div;
// }

showList(books);