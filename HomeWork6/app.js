/* Завдання */
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// Під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const findBtn = document.querySelector('button');
const result = document.createElement('div');
document.body.append(result);

findBtn.addEventListener('click', () => {
    getIP()
        .then(({ip}) => {
            getAddress(ip)
                .then(address => showAddress(ip, address))
        })
})

async function getData(url) {
    try {
        const response = await fetch(url);
        return (await response.json());
        
    } catch (error) {
        return new Error('Something went wrong!')
    }
}

function getIP() {
    return getData('https://api.ipify.org/?format=json');
}

function getAddress(ip) {
    return getData(`http://ip-api.com/json/${ip}?fields=1572885`);
}

    // якщо без загальної функції getdata (замість рядків 23-39):
// async function getIP() {
//     try {
//         const resp = await fetch('https://api.ipify.org/?format=json');
//         return resp.json();
//     } catch (error) {
//         return new Error('Something went wrong!')
//     }
// }
// async function getAddress(ip) {
//     try {
//         const res = await fetch(`http://ip-api.com/json/${ip}?fields=1572885`);
//         return res.json();        
//     } catch (error) {
//         return new Error('Something went wrong!')
//     }
// }

function showAddress(ip, data) {
    result.insertAdjacentHTML('beforeend', `<p> ip: ${ip} </p>`)
    for (const key in data) {
        const element = key;
        const value = data[key];
        if (data[key]) {
            result.insertAdjacentHTML('beforeend', `<p> ${element}: ${value} </p>`)
        } else {
            result.insertAdjacentHTML('beforeend', `<p> ${element}: No value for this parameter :( </p>`)
        }
    }
}
