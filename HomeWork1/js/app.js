// Завдання

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }

    getName() {
        return this.name;
    }

    setName(word) {
        this.name = word
    }
    
    getAge() {
        return this.age;
    }

    setAge(number) {
        this.age = number
    }

    getSalary() {
        return this.salary;
    }

    setSalary(value) {
        this.salary = value
    }
}

const mari = new Employee('Mari', 32, 35000)
console.log(mari);
mari.setName('Marianna');
console.log(mari.getName());
console.log(mari.getAge());
mari.setSalary(40000);
console.log(mari.getSalary());


class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    getSalary() {
        return this.salary * 3;
    }
}

const nikita = new Programmer('Nikita', 32, 25000, ['Ukr', 'Eng']);
console.log(nikita);
nikita.setAge(33);
console.log(nikita.getAge());
console.log(nikita.getSalary());

const vova = new Programmer('Volodymyr', 35, 30000, ['Ukr', 'Eng', 'Spain']);
console.log(vova);
vova.setName('Valdis');
console.log(vova.getName());
console.log(vova.getSalary());
