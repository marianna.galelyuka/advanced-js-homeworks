/* Завдання */
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.


const urlMain = 'https://ajax.test-danit.com/api/swapi/films';

function getFilms(url) {
    return fetch(url)
}

function getCharacters(url) {
    return fetch(url)
}

const filmsList = document.createElement('ul');

getFilms(urlMain)
    .then(data => data.json())
    .then(films => films.forEach((film) => {
        const filmsItem = document.createElement('li');
        // const pName = document.createElement('p');
        // pName.textContent = 'Film name: "' + film.name + '"';
        // const pEpisode = document.createElement('p');
        // pEpisode.textContent = 'Episode: ' + film.episodeId;
        // const pDescr = document.createElement('p');
        // pDescr.textContent = 'Short description: ' + film.openingCrawl;
        // filmsItem.append(pName, pEpisode, pDescr);
        filmsItem.insertAdjacentHTML ('afterbegin', `<p> <b>Film name: ${film.name} </b></p> <p>Episode: ${film.episodeId} </p><p> Short description: ${film.openingCrawl} </p>`)
        filmsList.append(filmsItem);
        document.body.append(filmsList)

        const actors = document.createElement('div');
        actors.insertAdjacentHTML('afterbegin', `<p> Actors: </p>`);
        filmsItem.insertAdjacentElement('beforeend', actors);
        const actorsList = document.createElement('ul');
        film.characters.forEach(character => {
            return getCharacters(character)
                .then(data => data.json())
                .then(actor => {
                    actorsList.insertAdjacentHTML('beforeend', `<li> ${actor.name}</li>`);
                })
            })
        actors.insertAdjacentElement('beforeend', actorsList);
    }))

